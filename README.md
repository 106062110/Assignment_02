## Topic
* Project Name : RAIDEN

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|
|Appearance|5%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Multi player game|5%|Y|
|Bullet automatic aiming|5%|Y|
|Unique bullet|5%|Y|

## Website Detail Description

# 作品網址：https://106062110.gitlab.io/Assignment_02/

# Components Description :
1. Complete game process : Start Menu  => 按下S開始1P模式，按下C開始2P模式
2. Complete game process : Game  View  => 1P模式 左右鍵移動、SPACE鍵攻擊、B鍵大絕 、2P模式 A、D鍵移動、S鍵攻擊、W鍵大絕
3. Complete game process : Game  Over  => 判定遊戲結束
4. Complete game process : Quit/Replay => 選擇重新挑戰 或 跳回主頁面
5. Basic rules : 玩家、敵人、地圖正常執行
6. Jucify mechanisms : Level => 當達到一定的分數，玩家的子彈數量會變多，且怪物的出怪速度以及數量、移動速度都會隨遊戲進行而增加。
7. Jucify mechanisms : Skill => 當玩家按下 B或Ｗ鍵，可以放大絕招，並在右下角會顯示大絕招的冷卻時間
8. Animations => 當玩家被敵方的子彈攻擊時，會有爆炸的animation產生
9. Particle Systems => 當子彈打到敵人時，會有Particle Systems特效，放大絕在畫面中心也會出現特效，以及玩家飛機後方有噴射氣流
10. Sound effects => 1-->增加BGM音量 2-->降低BGM音量 3-->增加子彈音量 4-->降低子彈音量
11. UI => Player health（右上角 ）Ultimate skill number or power（右下角）Score（左上角）volume control（左上角）Game pause(KEY P) （要多試幾次）
12. Appearance => 我自認可以拿五分 選我正解

# Other Functions Description(1~10%) :
1. Multi player game => 可以離線2P
2. Enhanced items : Bullet automatic aiming => 敵方戰機的子彈會自動瞄準我方戰機
3. Enhanced items : Unique bullet => 會掉下黃色子彈icon ，若成功吃到道具， 子彈會變成黃色，並且變成穿甲彈
