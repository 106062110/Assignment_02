var gameoverState = {
    preload: function()
    {
        game.load.image("starfield", "assets/starfield.png");
        game.load.image("ship", "assets/player.png");
        game.load.audio("bgm", "assets/tokyohot.mp3");
    },

    create: function()
    {
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');
        bgm = game.add.audio("bgm");
        bgm.loop = true;
        bgm.volume = 1;
        bgm.play();

        gameOver = game.add.text(game.world.centerX, game.world.centerY-30, 'GAME OVER!', { fontSize: 80, fill: '#fff' });
        gameOver.anchor.setTo(0.5, 0.5);

        playAgain = game.add.text(game.world.centerX, game.world.centerY+150, 'Play Again (Press Space)', { fontSize: 30, fill: '#fff' });
        playAgain.anchor.setTo(0.5, 0.5);

        quit = game.add.text(game.world.centerX, game.world.centerY+200, 'Quit (Press ESC)', { fontSize: 30, fill: '#fff' });
        quit.anchor.setTo(0.5, 0.5);

        spacebutton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        escbutton = game.input.keyboard.addKey(Phaser.Keyboard.ESC);
        volumeupButton = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        volumedownButton = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
    },

    update: function()
    {
        starfield.tilePosition.y += 2;

        if(spacebutton.isDown){
            bgm.stop();
            game.state.start('main');

        }
        if(escbutton.isDown){
            bgm.stop();
            game.state.start('start');
        }

        if(volumeupButton.isDown){
            if(bgm.volume >= 1) bgm.volume = 1;
            else bgm.volume += 0.01;
        }

        if(volumedownButton.isDown){
            if(bgm.volume <= 0) bgm.volume = 0;
            else bgm.volume -= 0.01;
        }
    }
};
