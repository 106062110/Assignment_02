var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

var player1;
var player2;
var starfield;
var cursors;
var ship1Trail;
var ship2Trail;
var bullets;
var redpoint;
var fireButton;
var fireBullet2;
var greenEnemies;
var blueEnemies;
var enemyBullets;
var explosions;
var bulletTimer2 = 0;
var ultimateBulletTimer2 = 0;
var ultimate;
var ultimate2;
var volume;
var center_firework;
var volumevalue = 100;
var gameOver;
var scoreText;
var pauseButton;
var specialbullets;
var greenEnemyLaunchTimer;
var blueEnemyLaunchTimer;
var pause = false;
var bulletTimer = 0;
var ultimate_flag1 = false;
var ultimate_flag2 = false;
var score = 0;
var cd1 = 100;
var cd1Times = 0;
var cd2 = 100;
var cd2Times = 0;
var pause_flag = 0;
var greenEnemySpacing = 1000;
var blueEnemyLaunched = false;
var usetool_1 = false;
var ultimateBulletTimer = 0;
var ultimateFireButton2;
var volume_con;
var volume_con2;
var levellimit1 = 6000;
var levellimit2 = 18000;
var REDPOINTOCCUR = 5000;
var launchBlueEnemylimit = 5000;

var mainState = {
    preload: function()
    {
        game.load.image("starfield", "assets/starfield.png");
        game.load.image("ship1", "assets/player1.png");
        game.load.image("ship2", "assets/player2.png");
        game.load.image("Redpoint", "assets/red_bullet_2_icon.png")
        game.load.image("bullet", "assets/bullet.png");
        game.load.image("specialbullet", "assets/red_bullet_2.png")
        game.load.image("firework_3", "assets/firework_3.png");
        game.load.image("firework_5", "assets/firework_5.png");
        game.load.image("firework_6", "assets/firework_6.png")
        game.load.image("firework_7", "assets/firework_7.png")

        game.load.image('blueEnemyBullet', 'assets/enemy-blue-bullet.png');
        game.load.image("enemy-green", "assets/enemy1.png");
        game.load.image("enemy-blue", "assets/enemy2.png");
        game.load.spritesheet('explosion', 'assets/explode.png', 128, 128);
        game.load.audio("laser_audio", "assets/laser1.mp3");
        game.load.audio("bgm", "assets/tokyohot.mp3");

    },
    create: function()
    {
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

        bgm = game.add.audio("bgm");
        bgm.loop = true;
        bgm.volume = 1;
        bgm.play();

        audio_eff = game.add.audio("laser_audio");
        audio_eff.volume = 0.3;

        cursors = game.input.keyboard.createCursorKeys();


        key_a = game.input.keyboard.addKey(Phaser.Keyboard.A);
        key_d = game.input.keyboard.addKey(Phaser.Keyboard.D);
        fireButton2 = game.input.keyboard.addKey(Phaser.Keyboard.S);
        ultimateFireButton2 = game.input.keyboard.addKey(Phaser.Keyboard.W);

        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        ultimateFireButton = game.input.keyboard.addKey(Phaser.Keyboard.B);
        volumeupButton = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        volumedownButton = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        bulletsvolumeupButton = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        bulletsvolumedownButton = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        pauseButton = game.input.keyboard.addKey(Phaser.Keyboard.P);


        //player1
        player1 = game.add.sprite(600, 500, 'ship1');
        player1.health = 100;
        player1.weaponLevel = 1;
        player1.anchor.setTo(0.5, 0.5);
        game.physics.enable(player1, Phaser.Physics.ARCADE);
        player1.events.onKilled.add(function(){
            ship1Trail.kill();
        });
        player1.events.onRevived.add(function(){
            ship1Trail.start(false, 5000, 10);
        });

        //player2
        player2 = game.add.sprite(200, 500, 'ship2');
        player2.health = 100;
        player2.weaponLevel = 1;
        player2.anchor.setTo(0.5, 0.5);
        game.physics.enable(player2, Phaser.Physics.ARCADE);
        player2.events.onKilled.add(function(){
            ship2Trail.kill();
        });
        player2.events.onRevived.add(function(){
            ship2Trail.start(false, 5000, 10);
        });

        //blue bullet
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(200, 'bullet');
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

        //red bullet
        specialbullets = game.add.group();
        specialbullets.enableBody = true;
        specialbullets.physicsBodyType = Phaser.Physics.ARCADE;
        specialbullets.createMultiple(200, 'specialbullet');
        specialbullets.setAll('anchor.x', 0.5);
        specialbullets.setAll('anchor.y', 1);
        specialbullets.setAll('outOfBoundsKill', true);
        specialbullets.setAll('checkWorldBounds', true);

        //enemy bullet
        blueEnemyBullets = game.add.group();
        blueEnemyBullets.enableBody = true;
        blueEnemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        blueEnemyBullets.createMultiple(100, 'blueEnemyBullet');
        blueEnemyBullets.callAll('crop', null, {x: 90, y: 0, width: 90, height: 70});
        blueEnemyBullets.setAll('anchor.x', 0.5);
        blueEnemyBullets.setAll('anchor.y', 0.5);
        blueEnemyBullets.setAll('outOfBoundsKill', true);
        blueEnemyBullets.setAll('checkWorldBounds', true);
        blueEnemyBullets.forEach(function(enemy){
            enemy.body.setSize(50, 50);
        });

        //greenenemy
        greenEnemies = game.add.group();
        greenEnemies.enableBody = true;
        greenEnemies.physicsBodyType = Phaser.Physics.ARCADE;
        greenEnemies.createMultiple(100, 'enemy-green');
        greenEnemies.setAll('anchor.x', 0.5);
        greenEnemies.setAll('anchor.y', 0.5);
        greenEnemies.setAll('scale.x', 0.5);
        greenEnemies.setAll('scale.y', 0.5);
        greenEnemies.setAll('angle', 180);
        greenEnemies.setAll('outOfBoundsKill', true);
        greenEnemies.setAll('checkWorldBounds', true);
        greenEnemies.forEach(function(enemy){
            enemy.body.setSize(enemy.width * 3 / 4, enemy.height * 3 / 4);
            enemy.damageAmount = 10;
            enemy.emitter = game.add.emitter(0, 0, 10);
            enemy.emitter.makeParticles('firework_6');
            enemy.emitter.setScale(2, 0, 2, 0, 1000);
            enemy.emitter.gravity = 0;
        });
        game.time.events.add(500, launchGreenEnemy);

        //blueenemy
        blueEnemies = game.add.group();
        blueEnemies.enableBody = true;
        blueEnemies.physicsBodyType = Phaser.Physics.ARCADE;
        blueEnemies.createMultiple(100, 'enemy-blue');
        blueEnemies.setAll('anchor.x', 0.5);
        blueEnemies.setAll('anchor.y', 0.5);
        blueEnemies.setAll('scale.x', 0.5);
        blueEnemies.setAll('scale.y', 0.5);
        blueEnemies.setAll('angle', 180);
        blueEnemies.forEach(function(enemy){
            enemy.body.setSize(enemy.width * 3 / 4, enemy.height * 3 / 4);
            enemy.damageAmount = 20;
            enemy.emitter = game.add.emitter(0, 0, 10);
            enemy.emitter.makeParticles('firework_5');
            enemy.emitter.setScale(2, 0, 2, 0, 1000);
            enemy.emitter.gravity = 0;
        });

        ship1Trail = game.add.emitter(player1.x, player1.y + 10, 400);
        ship1Trail.width = 10;
        ship1Trail.makeParticles("firework_3");
        ship1Trail.setXSpeed(30, -30);
        ship1Trail.setYSpeed(200, 180);
        ship1Trail.setRotation(50, -50);
        ship1Trail.setAlpha(1, 0.01, 800);
        ship1Trail.setScale(0.05, 0.4, 0.05, 0.4, 2000, Phaser.Easing.Quintic.Out);
        ship1Trail.start(false, 5000, 10);

        ship2Trail = game.add.emitter(player2.x, player2.y + 10, 400);
        ship2Trail.width = 10;
        ship2Trail.makeParticles("bullet");
        ship2Trail.setXSpeed(30, -30);
        ship2Trail.setYSpeed(200, 180);
        ship2Trail.setRotation(50, -50);
        ship2Trail.setAlpha(1, 0.01, 800);
        ship2Trail.setScale(0.05, 0.4, 0.05, 0.4, 2000, Phaser.Easing.Quintic.Out);
        ship2Trail.start(false, 5000, 10);

        explosions = game.add.group();
        explosions.enableBody = true;
        explosions.physicsBodyType = Phaser.Physics.ARCADE;
        explosions.createMultiple(30, 'explosion');
        explosions.setAll('anchor.x', 0.5);
        explosions.setAll('anchor.y', 0.5);
        explosions.forEach( function(explosion) {
            explosion.animations.add('explosion');
        });


        player1.shields = game.add.text(game.world.width - 220, 10, '', { fontSize: 32,fill: '#fff' });
        player1.shields.visible = true;
        player1.shields.render = function () {
            player1.shields.text = 'Player1: ' + Math.max(player1.health, 0) +'%';
        };
        player1.shields.render();

        player2.shields = game.add.text(game.world.width - 220, 45, '', { fontSize: 32,fill: '#fff' });
        player2.shields.visible = true;
        player2.shields.render = function () {
            player2.shields.text = 'Player2: ' + Math.max(player2.health, 0) +'%';
        };
        player2.shields.render();

        player1.ultimate = game.add.text(game.world.width - 210, 570, '', { fontSize: 24,fill: '#fff' });
        player1.ultimate.visible = true;
        player1.ultimate.render = function () {
            player1.ultimate.text = "Player1_CD: " + cd1 +'%';
        };
        player1.ultimate.render();

        player2.ultimate = game.add.text(game.world.width - 210, 540, '', { fontSize: 24,fill: '#fff' });
        player2.ultimate.visible = true;
        player2.ultimate.render = function () {
            player2.ultimate.text = "Player2_CD: " + cd2 +'%';
        };
        player2.ultimate.render();

        volume = game.add.text(10, 50, '', { fontSize: 24,fill: '#fff' });
        volume.visible = true;
        volume.render = function () {
            volume.text = "Volume : " + volumevalue + '%';
        };
        volume.render();

        volume_con = game.add.text(10, 560, 'bgm 1+2-', { fontSize: 16,fill: '#fff' });
        volume_con.visible = true;

        volume_con2 = game.add.text(10, 580, 'bullet sound 3+4-', { fontSize: 16,fill: '#fff' });
        volume_con2.visible = true;

        // player1.attack_text_1 = game.add.text(300, 580, 'player1: Space && B', { fontSize: 16,fill: '#fff' });
        // player1.attack_text_1.visible = true;
        //
        // player2.attack_text_2 = game.add.text(300, 560, 'player2: S && W', { fontSize: 16,fill: '#fff' });
        // player2.attack_text_2.visible = true;

        scoreText = game.add.text(10, 10, '', {fontSize: 32, fill: '#fff'});
        scoreText.visible = true;
        scoreText.render = function () {
            scoreText.text = 'Score: ' + score;
        };
        scoreText.render();

        pause_text = game.add.text(game.world.centerX, game.world.centerY, 'PAUSE', {  fontSize: 80, fill: '#fff' });
        pause_text.anchor.setTo(0.5, 0.5);
        pause_text.visible = false;
    },

    update: function()
    {
        if(!whosyourdaddynow){
            player2.kill();
        }
        starfield.tilePosition.y += 2;
        ship1Trail.x = player1.x;
        ship2Trail.x = player2.x;

        pauseButton.onDown.add(function () {
            pause = !pause;
            if (pause){
                game.paused = true;
                pause_text.visible = true;
            }
            else{
                game.paused = false;
                pause_text.visible = false;
            }
        }, this);




        if (cursors.left.isDown) player1.body.x += -5;
        else if (cursors.right.isDown) player1.body.x += 5;

        if (key_a.isDown) player2.body.x += -5;
        else if (key_d.isDown) player2.body.x += 5;

        // if (key_a.isDown) player2.body.velocity.x = -200;
        // else if (key_d.isDown) player2.body.velocity.x = 200;

        if (player1.x > game.width - 20) player1.x = game.width - 20;

        if (player1.x < 20) player1.x = 20;

        if (player2.x > game.width - 20) player2.x = game.width - 20;

        if (player2.x < 20) player2.x = 20;

        if(!player1.alive){
            player1.shields.visible = false;
            player1.ultimate.visible = false;
        }

        if(!player2.alive){
            player2.shields.visible = false;
            player2.ultimate.visible = false;
        }
        if(score > REDPOINTOCCUR && usetool_1 === false){

            //
            redpoint = game.add.sprite(game.rnd.integerInRange(100, 700), 50, 'Redpoint');
            redpoint.anchor.setTo(0.5, 0.5);

            // redpoint.set('outOfBoundsKill', true);
            // redpoint.set('checkWorldBounds', true);
            game.physics.enable(redpoint, Phaser.Physics.ARCADE);
            redpoint.body.velocity.y = 250;
            //
            REDPOINTOCCUR += 5000;

        }

        if (player1.alive && fireButton.isDown) fireBullet();

        if (player1.alive && ultimateFireButton.isDown){
            ultimate_flag1 = true;
            fireBullet();
        }

        if (player2.alive && fireButton2.isDown) fireBullet2();
        if (player2.alive && ultimateFireButton2.isDown){
            ultimate_flag2 = true;
            fireBullet2();
        }

        if (game.time.now > cd1Times){
            if(cd1 === 100) cd1 = 100;
            else cd1 += 1;
            cd1Times = game.time.now + 85;
            player1.ultimate.render();
        }

        if (game.time.now > cd2Times){
            if(cd2 === 100) cd2 = 100;
            else cd2 += 1;
            cd2Times = game.time.now + 85;
            player2.ultimate.render();
        }

        if(!player1.alive && !player2.alive){
            restart();
            bgm.stop();
            game.state.start('gameover');
        }

        // UI setting
        if(volumeupButton.isDown){
            if(bgm.volume >= 1) bgm.volume = 1;
            else bgm.volume += 0.01;
            volumevalue = Math.ceil(bgm.volume * 100);
            volume.render();
        }

        if(volumedownButton.isDown){
            if(bgm.volume <= 0) bgm.volume = 0;
            else bgm.volume -= 0.01;
            volumevalue = Math.ceil(bgm.volume * 100);
            volume.render();
        }

        if(bulletsvolumeupButton.isDown){
            if(audio_eff.volume >= 1) audio_eff.volume = 1;
            else audio_eff.volume += 0.01;
        }

        if(bulletsvolumedownButton.isDown){
            if(audio_eff.volume <= 0) audio_eff.volume = 0;
            else audio_eff.volume -= 0.01;
        }
        // end UI setting

        game.physics.arcade.overlap(redpoint, player1, usetool1, null, this);
        game.physics.arcade.overlap(redpoint, player2, usetool1, null, this);
        game.physics.arcade.overlap(player1, greenEnemies, shipCollide, null, this);
        game.physics.arcade.overlap(player1, blueEnemies, shipCollide, null, this);
        game.physics.arcade.overlap(player2, greenEnemies, shipCollide, null, this);
        game.physics.arcade.overlap(player2, blueEnemies, shipCollide, null, this);
        game.physics.arcade.overlap(greenEnemies, bullets, hitEnemy, null, this);
        game.physics.arcade.overlap(blueEnemies, bullets, hitEnemy, null, this);
        game.physics.arcade.overlap(greenEnemies, specialbullets, hitEnemy, null, this);
        game.physics.arcade.overlap(blueEnemies, specialbullets, hitEnemy, null, this);
        game.physics.arcade.overlap(blueEnemyBullets, player1, enemyHitsplayer, null, this);
        game.physics.arcade.overlap(blueEnemyBullets, player2, enemyHitsplayer, null, this);
    }
};

function usetool1(){
    if(redpoint.visible === true) usetool_1 = true;
    redpoint.visible = false;
    redpoint.kill();
    // center_firework.start(true, 800, null, 15);
}

function fireBullet2() {
    if (ultimate_flag2) {
        if (game.time.now > ultimateBulletTimer2) {
            cd2 = 0;
            audio_eff.play();

            center_firework = game.add.emitter(0, 0, 5);
            center_firework.makeParticles('firework_7');
            center_firework.setScale(2, 0, 2, 0, 1000);
            center_firework.gravity = 0;
            center_firework.x = game.world.centerX;
            center_firework.y = game.world.centerY;
            center_firework.start(true, 800, null, 15);


            ultimateBulletTimer2 = game.time.now + 10000;
            for (var i = 0; i < 100; i++) {
                if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                else var bullet = bullets.getFirstExists(false);
                if (bullet) {
                    bullet.reset(player2.x, player2.y + 8);;
                    bullet.body.velocity.y = -400;
                    if (i < 50) bullet.body.velocity.x = -25*i;
                    else bullet.body.velocity.x =  25*(i-50);
                }
            }
        }
        ultimate_flag2 = false;
    }
    else{
        switch(player1.weaponLevel){
            case 1:
                if(game.time.now > bulletTimer2){
                    if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                    else var bullet = bullets.getFirstExists(false);

                    if (bullet){
                        bullet.reset(player2.x, player2.y + 8);
                        bullet.body.velocity.y = -400;
                    }
                    bulletTimer2 = game.time.now + 300;
                    audio_eff.play();
                }

            case 2:
                if (game.time.now > bulletTimer2) {
                    for (var i = 0; i < 3; i++) {
                        if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                        else var bullet = bullets.getFirstExists(false);

                        if (bullet) {
                            bullet.reset(player2.x, player2.y + 8);
                            bullet.body.velocity.y = -400;
                            if (i === 0) bullet.body.velocity.x = -50;
                            if (i === 2) bullet.body.velocity.x = 50;
                        }
                    }
                    bulletTimer2 = game.time.now + 500;
                    audio_eff.play();
                }

            case 3:
                if (game.time.now > bulletTimer2) {
                    for (var i = 0; i < 5; i++) {
                        if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                        else var bullet = bullets.getFirstExists(false);

                        if (bullet) {
                            bullet.reset(player2.x, player2.y + 8);
                            bullet.body.velocity.y = -400;
                            if (i === 0) bullet.body.velocity.x = -50;
                            if (i === 1) bullet.body.velocity.x = -25;
                            if (i === 3) bullet.body.velocity.x = 25;
                            if (i === 4) bullet.body.velocity.x = 50;
                        }
                    }
                    bulletTimer2 = game.time.now + 600;
                    audio_eff.play();
                }
        }
    }
}

function fireBullet() {
    if (ultimate_flag1) {
        if (game.time.now > ultimateBulletTimer) {
            cd1 = 0;
            audio_eff.play();
            center_firework = game.add.emitter(0, 0, 5);
            center_firework.makeParticles('firework_7');
            center_firework.setScale(2, 0, 2, 0, 1000);
            center_firework.gravity = 0;
            center_firework.x = game.world.centerX;
            center_firework.y = game.world.centerY;
            center_firework.start(true, 800, null, 15);
            ultimateBulletTimer = game.time.now + 10000;
            for (var i = 0; i < 100; i++) {
                if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                else var bullet = bullets.getFirstExists(false);
                if (bullet) {
                    bullet.reset(player1.x, player1.y + 8);;
                    bullet.body.velocity.y = -400;
                    if (i < 50) bullet.body.velocity.x = -25*i;
                    else bullet.body.velocity.x =  25*(i-50);
                }
            }
        }
        ultimate_flag1 = false;
    }
    else{
        switch(player1.weaponLevel){
            case 1:
                if(game.time.now > bulletTimer){
                    if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                    else var bullet = bullets.getFirstExists(false);

                    if (bullet){
                        bullet.reset(player1.x, player1.y + 8);
                        bullet.body.velocity.y = -400;
                    }
                    bulletTimer = game.time.now + 300;
                    audio_eff.play();
                }

            case 2:
                if (game.time.now > bulletTimer) {
                    for (var i = 0; i < 3; i++) {
                        if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                        else var bullet = bullets.getFirstExists(false);

                        if (bullet) {
                            bullet.reset(player1.x, player1.y + 8);
                            bullet.body.velocity.y = -400;
                            if (i === 0) bullet.body.velocity.x = -50;
                            if (i === 2) bullet.body.velocity.x = 50;
                        }
                    }
                    bulletTimer = game.time.now + 500;
                    audio_eff.play();
                }

            case 3:
                if (game.time.now > bulletTimer) {
                    for (var i = 0; i < 5; i++) {
                        if(usetool_1) var bullet = specialbullets.getFirstExists(false);
                        else var bullet = bullets.getFirstExists(false);

                        if (bullet) {
                            bullet.reset(player1.x, player1.y + 8);
                            bullet.body.velocity.y = -400;
                            if (i === 0) bullet.body.velocity.x = -50;
                            if (i === 1) bullet.body.velocity.x = -25;
                            if (i === 3) bullet.body.velocity.x = 25;
                            if (i === 4) bullet.body.velocity.x = 50;
                        }
                    }
                    bulletTimer = game.time.now + 600;
                    audio_eff.play();
                }
        }
    }
}


function launchGreenEnemy() {

    var ENEMY_SPEED = 300;

    if(score > 10000) ENEMY_SPEED = 330;
    if(score > 25000) ENEMY_SPEED = 360;
    if(score > 35000) ENEMY_SPEED = 390;
    if(score > 40000) ENEMY_SPEED = 420;
    if(score > 45000) ENEMY_SPEED = 450;
    if(score > 50000) ENEMY_SPEED = 480;
    if(score > 55000) ENEMY_SPEED = 520;
    if(score > 60000) ENEMY_SPEED = 550;
    if(score > 65000) ENEMY_SPEED = 580;
    if(score > 70000) ENEMY_SPEED = 600;

    var enemy = greenEnemies.getFirstExists(false);
    if (enemy) {
        enemy.reset(game.rnd.integerInRange(0, game.width), -20);
        enemy.body.velocity.x = game.rnd.integerInRange(-300, 300);
        enemy.body.velocity.y = ENEMY_SPEED;
        enemy.body.drag.x = 100;
        enemy.update = function(){
            if(enemy.y > game.height + 200){
                enemy.kill();
            }
        }
    }
    greenEnemyLaunchTimer = game.time.events.add(game.rnd.integerInRange(greenEnemySpacing, greenEnemySpacing + 1000), launchGreenEnemy);

}

function launchBlueEnemy() {
    var startingX = game.rnd.integerInRange(100, game.width - 100);
    var verticalSpeed = 180;

    if(score > 10000) verticalSpeed = 180;
    if(score > 25000) verticalSpeed = 200;
    if(score > 35000) verticalSpeed = 220;
    if(score > 40000) verticalSpeed = 240;
    if(score > 45000) verticalSpeed = 260;
    if(score > 50000) verticalSpeed = 300;
    if(score > 55000) verticalSpeed = 330;
    if(score > 60000) verticalSpeed = 360;
    if(score > 70000) verticalSpeed = 400;

    var spread = 60;
    var frequency = 70;
    var verticalSpacing = 70;
    var numEnemiesInWave;
    var timeBetweenWaves = 3000;

    if(score < 10000) numEnemiesInWave = 5;
    else numEnemiesInWave = Math.ceil((score - 10000) / 10000) + 6;

    for (var i = 0; i < numEnemiesInWave; i++) {
        var enemy = blueEnemies.getFirstExists(false);
        if (enemy) {
            enemy.damageAmount = 10;
            enemy.startingX = startingX;
            enemy.reset(game.width / 2, -verticalSpacing * i);
            enemy.body.velocity.y = verticalSpeed;

            var bulletSpeed = 400;
            var firingDelay = 2000;
            enemy.bullets = 1;
            enemy.lastShot = 0;

            enemy.update = function(){
                this.body.x = this.startingX + Math.sin((this.y) / frequency) * spread;
                enemyBullet = blueEnemyBullets.getFirstExists(false);
                if (enemyBullet && this.alive && this.bullets && this.y > game.width / 8 &&
                game.time.now > firingDelay + this.lastShot) {
                    this.lastShot = game.time.now;
                    this.bullets--;
                    enemyBullet.reset(this.x, this.y + this.height / 2);
                    enemyBullet.damageAmount = this.damageAmount;

                    var angle;
                    var tmp_num = game.rnd.integerInRange(100, 200);
                    if (tmp_num < 150 && player1.alive) angle = game.physics.arcade.moveToObject(enemyBullet, player1, bulletSpeed);
                    else if(tmp_num < 150 && !player1.alive) angle = game.physics.arcade.moveToObject(enemyBullet, player2, bulletSpeed);
                    else if (tmp_num >= 150 && player2.alive) angle = game.physics.arcade.moveToObject(enemyBullet, player2, bulletSpeed);
                    else if(tmp_num >= 150 && !player2.alive) angle = game.physics.arcade.moveToObject(enemyBullet, player1, bulletSpeed);

                    enemyBullet.angle = game.math.radToDeg(angle);
                }

                if (this.y > game.height + 200) {
                    this.kill();
                    this.y = -20;
                }
            };
        }
    }

    timeBetweenWaves *= 0.8;
    blueEnemyLaunchTimer = game.time.events.add(game.rnd.integerInRange(timeBetweenWaves, timeBetweenWaves + 2000), launchBlueEnemy);
}

function shipCollide(player, enemy) {
    var explosion = explosions.getFirstExists(false);
    if(!explosion) return;
    explosion.reset(enemy.body.x + enemy.body.halfWidth, enemy.body.y + enemy.body.halfHeight);
    explosion.body.velocity.y = enemy.body.velocity.y;
    explosion.alpha = 0.7;
    explosion.play('explosion', 30, false, true);
    enemy.kill();
    player.damage(enemy.damageAmount);
    player.shields.render();
}

function hitEnemy(enemy, bullet) {
    enemy.kill();

    // var explosion = explosions.getFirstExists(false);
    // if(!explosion) return;

    enemy.emitter.x = enemy.x;
    enemy.emitter.y = enemy.y;
    enemy.emitter.start(true, 800, null, 15);

    // explosion.reset(bullet.body.x + bullet.body.halfWidth, bullet.body.y + bullet.body.halfHeight);
    // explosion.body.velocity.y = enemy.body.velocity.y;
    // explosion.alpha = 0.7;
    // explosion.play('explosion', 30, false, true);

    if(usetool_1 === false) bullet.kill();

    score += enemy.damageAmount * 10;
    scoreText.render();
    greenEnemySpacing *= 0.8;

    if (!blueEnemyLaunched && score > launchBlueEnemylimit) {
      blueEnemyLaunched = true;
      launchBlueEnemy();
    }

    if (score > levellimit1 && player1.weaponLevel < 2) player1.weaponLevel = 2;
    if (score > levellimit2 && player1.weaponLevel < 3) player1.weaponLevel = 3;

}

function enemyHitsplayer (player, bullet) {
    var explosion = explosions.getFirstExists(false);
    if(!explosion) return;
    explosion.reset(player.body.x + player.body.halfWidth, player.body.y + player.body.halfHeight);
    explosion.alpha = 0.7;
    explosion.play('explosion', 30, false, true);
    bullet.kill();
    player.damage(bullet.damageAmount);
    player.shields.render()
}

function restart () {
    greenEnemies.callAll('kill');
    game.time.events.remove(greenEnemyLaunchTimer);
    game.time.events.remove(blueEnemyLaunchTimer);
    game.time.events.add(1000, launchGreenEnemy);
    blueEnemyBullets.callAll('kill');
    blueEnemies.callAll('kill');

    player1.revive();
    player1.health = 100;
    player1.shields.render();
    cd1 = 100;

    player2.revive();
    player2.health = 100;
    player2.shields.render();
    cd2 = 100;

    score = 0;
    scoreText.render();
    player1.weaponLevel = 1;
    greenEnemySpacing = 1000;
    blueEnemyLaunched = false;
    bulletTimer2 = 0;
    ultimateBulletTimer2 = 0;
    bulletTimer = 0;
    ultimate_flag1 = false;
    ultimate_flag2 = false;
    cd1Times = 0;
    cd2Times = 0;

    usetool_1 = false;
    REDPOINTOCCUR = 5000;
    ultimateBulletTimer = 0;
}

game.state.add('main', mainState);
game.state.add('start', startState);
game.state.add('gameover', gameoverState);
game.state.start('start');
