var whosyourdaddynow;
var startState = {
    preload: function()
    {
        game.load.image("starfield", "assets/starfield.png");
        game.load.image("ship", "assets/player.png");
        game.load.audio("bgm", "assets/tokyohot.mp3");
    },

    create: function()
    {
        starfield = game.add.tileSprite(0, 0, 800, 600, 'starfield');

        bgm = game.add.audio("bgm");
        bgm.loop = true;
        bgm.volume = 1;
        bgm.play();

        raiden = game.add.text(game.world.centerX, game.world.centerY, 'RAIDEN', {  fontSize: 80, fill: '#fff' });
        raiden.anchor.setTo(0.5, 0.5);

        start1p = game.add.text(game.world.centerX, game.world.centerY + 100, 'One Player  (Press s)', { fill: '#fff' });
        start1p.anchor.setTo(0.5, 0.5);
        start2p = game.add.text(game.world.centerX, game.world.centerY + 150, 'Two Players (Press c)', { fill: '#fff' });
        start2p.anchor.setTo(0.5, 0.5);

        enterbutton1p = game.input.keyboard.addKey(Phaser.Keyboard.S);
        enterbutton2p = game.input.keyboard.addKey(Phaser.Keyboard.C);
        volumeupButton = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        volumedownButton = game.input.keyboard.addKey(Phaser.Keyboard.TWO);

    },

    update: function()
    {
        starfield.tilePosition.y += 2;

        if(enterbutton1p.isDown){
            bgm.stop();
            game.state.start('main');
            whosyourdaddynow = false;

        }

        if(enterbutton2p.isDown){
            bgm.stop();
            game.state.start('main');
            whosyourdaddynow = true;
        }

        if(volumeupButton.isDown){
            if(bgm.volume >= 1) bgm.volume = 1;
            else bgm.volume += 0.01;
        }

        if(volumedownButton.isDown){
            if(bgm.volume <= 0) bgm.volume = 0;
            else bgm.volume -= 0.01;
        }
    }
};
